<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{integralces}prestashop>integralces_d4f42f557ea8b5914d64dba492970b53'] = 'Moneda Social';
$_MODULE['<{integralces}prestashop>integralces_2da1dc81dbb180956d6e466b376a369a'] = 'Aceptar pagos con moneda social utilizando sistemas como integralCES o CES';
$_MODULE['<{integralces}prestashop>integralces_3d380f8f3d9c42ebd0a39e275ac985ba'] = 'Pago utilizando Moneda Social';
$_MODULE['<{integralces}prestashop>integralces_intro_04d3fe82a4cba45493e685c18bb5f829'] = 'Cobraremos el pago con su moneda social si tiene cuenta en IntegralCES o CES';
$_MODULE['<{integralces}prestashop>payment_return_88526efe38fd18179a127024aba8c1d7'] = 'Su pedido,  %s, está completo.';
$_MODULE['<{integralces}prestashop>payment_return_e6dc7945b557a1cd949bea92dd58963e'] = 'Su pedido será enviado muy pronto.';
$_MODULE['<{integralces}prestashop>payment_return_0db71da7150c27142eef9d22b843b4a9'] = 'Para cualquier duda, pregunta o más información, por favor contáctenos';
$_MODULE['<{integralces}prestashop>payment_return_64430ad2835be8ad60c59e7d44e4b0b1'] = 'Servicio al intercambio';
